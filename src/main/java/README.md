
# Project Title

The Project is about Movie, in this Project we can add Movie to the

database, we can search Movie (by its author, content and name),

we can take list of Movies from the database, we can update the

particular Movie's field, and we can delete the Movie from database.

In this Project I used: 
java 11,
PostgreSQL for database, 
Liquibase for migration,
Hibernate for ORM

#Instruction for MovieService

1. MovieItem add(MovieForm postForm);
   example:
   POST request: localhost:8080/api/v1/movie
   {
   "name": "World War",
   "author": "Luka Toni",
   "content": "Action",
   "company": "Marvel"
   }

2. List<MovieListItem> list();
   example
   GET request : localhost:8080/api/v1/movie
   MovieListItem is created by Movie entity.

3. Page<Movie> search(Pageable pageable, String text);
   example
   GET request : localhost:8080/api/v1/movie/"___"
   response is Page of Movies

4.MovieItem update(Long id, Form form);
example
PATCH request: localhost:8080/api/v1/movie/id;
{
"company" : "_____"
}
//in this method we can only change the company of the movie

5.boolean delete(Long id);
example
DELETE request: localhost:8080/api/v1/movie/id;
//in this method we delete the Movie from the database by its id.
