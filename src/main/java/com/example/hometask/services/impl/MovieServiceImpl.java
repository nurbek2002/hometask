package com.example.hometask.services.impl;

import com.example.hometask.entities.Movie;
import com.example.hometask.model.*;
import com.example.hometask.repostory.MovieRepository;
import com.example.hometask.services.MovieService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository repository;

    public MovieServiceImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<MovieListItem> list() {
        return repository.findAll()
                .stream()
                .map(MovieListItem::fromMovie).collect(Collectors.toList());
    }

    @Override
    public Page<Movie> search(Pageable pageable, String text) {
        return repository.searchUsingAnyText(pageable, text);
    }

    @Override
    public MovieItem add(MovieForm movieForm) {
        Movie movie = Movie.from(movieForm);
        repository.save(movie);
        return MovieItem.fromMovie(movie);
    }

    @Override
    public MovieItem update(Long id, Form form) {
        Movie movie = repository.getById(id);
        movie.setCompany(form.getCompany());
        return MovieItem.fromMovie(repository.save(movie));
    }

    @Override
    public boolean delete(Long id) {
        repository.deleteById(id);
        return true;
    }
}
