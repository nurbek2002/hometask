package com.example.hometask.services;

import com.example.hometask.entities.Movie;
import com.example.hometask.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MovieService {

    List<MovieListItem> list();

    Page<Movie> search(Pageable pageable, String text);

    MovieItem add(MovieForm postForm);

    MovieItem update(Long id, Form form);

    boolean delete(Long id);

}
