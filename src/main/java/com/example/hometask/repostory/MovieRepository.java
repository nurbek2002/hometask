package com.example.hometask.repostory;

import com.example.hometask.entities.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    @Query(value = "select * " +
            "from movie m " +
            "where author like %:text% " +
            "or content like %:text% " +
            "or  name like %:text% " +
            "order by name", nativeQuery = true)
    Page<Movie> searchUsingAnyText(Pageable pageable, String text);

}
