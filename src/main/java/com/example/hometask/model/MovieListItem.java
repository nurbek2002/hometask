package com.example.hometask.model;

import com.example.hometask.entities.Movie;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieListItem {

    private Long id;

    private String name;

    private String author;

    private String content;

    public static MovieListItem fromMovie(Movie movie) {
        MovieListItem item = new MovieListItem();
        item.setId(movie.getId());
        item.setName(movie.getName());
        item.setAuthor(movie.getAuthor());
        item.setContent(movie.getContent());
        return item;
    }

}
