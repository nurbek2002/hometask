package com.example.hometask.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieForm {

    @NotNull
    private String name;

    @NotNull
    private String author;

    @NotNull
    private String content;

    @NotNull
    private String company;

}
