package com.example.hometask.model;

import com.example.hometask.entities.Movie;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieItem {

    private Long id;

    private String name;

    private String author;

    private String content;

    private String company;

    public static MovieItem fromMovie(Movie movie){
        MovieItem item = new MovieItem();
        item.setId(movie.getId());
        item.setName(movie.getName());
        item.setContent(movie.getContent());
        item.setAuthor(movie.getAuthor());
        item.setCompany(movie.getCompany());
        return item;
    }

}
