package com.example.hometask.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Form {

    @NotNull
    private String company;

}
