package com.example.hometask.entities;

import com.example.hometask.model.MovieForm;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String author;

    @Column(columnDefinition = "text")
    private String content;

    private String company;

    public static Movie from(MovieForm movieForm) {
        Movie movie = new Movie();
        movie.setName(movieForm.getName());
        movie.setAuthor(movieForm.getAuthor());
        movie.setContent(movieForm.getContent());
        movie.setCompany(movieForm.getCompany());
        return movie;
    }

}
