package com.example.hometask.controller;

import com.example.hometask.model.Form;
import com.example.hometask.model.MovieForm;
import com.example.hometask.services.MovieService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;


@RestController
@RequestMapping("api/v1/movie")
public class MovieController {

    private final MovieService service;

    public MovieController(MovieService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<?> list() {
        return ResponseEntity.accepted().body(service.list());
    }

    @GetMapping("{text}")
    public ResponseEntity<?> search(Pageable pageable, @PathVariable String text) {
        return ResponseEntity.accepted().body(service.search(pageable, text));
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody @Validated MovieForm movieForm) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/v1/movie").toUriString());
        return ResponseEntity.created(uri).body(service.add(movieForm));
    }

    @PatchMapping("{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody @Validated Form form) {
        return ResponseEntity.ok(service.update(id, form));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(service.delete(id));
    }
}
